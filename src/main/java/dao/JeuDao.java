package dao;


import entity.Jeu;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class JeuDao {

    public Integer save(Jeu jeu) {
        org.hibernate.Session session = HibernateUtil.getCurrentSession();
        return (Integer) session.save(jeu);
    }
    public Jeu load(Integer id) {
        Session session = null;
        session = HibernateUtil.getCurrentSession();
        return (Jeu) session.get(Jeu.class, id);
    }
    public void remove(Jeu jeu) {
        Session session = HibernateUtil.getCurrentSession();
        session.delete(jeu);
    }
    public List getAll() {
        Session session = HibernateUtil.getCurrentSession();
        List jeux;
        Query q = session.createQuery("from Jeu");
        jeux = q.list();
        return jeux;
    }
    public Jeu getJeuFromName(String nom) {
        Session session = HibernateUtil.getCurrentSession();
        Query q = session.createQuery("From Jeu j " + "where j.nom=:name");
        q.setParameter("name", nom);
        return (Jeu) q.uniqueResult();
    }
}
