package entity;

public class Jeu {
    private Integer id;
    private String nom;
    private String editeur;
    private int ageMin;
    private int tpsMoyen;
    private int nbJoueursMin;
    private int nbJoueursMax;

    public Jeu() {
    }

    public Jeu(Integer id, String nom, String editeur, int ageMin, int tpsMoyen, int nbJoueursMin, int nbJoueursMax) {
        this.id = id;
        this.nom = nom;
        this.editeur = editeur;
        this.ageMin = ageMin;
        this.tpsMoyen = tpsMoyen;
        this.nbJoueursMin = nbJoueursMin;
        this.nbJoueursMax = nbJoueursMax;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEditeur() {
        return editeur;
    }

    public void setEditeur(String editeur) {
        this.editeur = editeur;
    }

    public int getAgeMin() {
        return ageMin;
    }

    public void setAgeMin(int ageMin) {
        this.ageMin = ageMin;
    }

    public int getTpsMoyen() {
        return tpsMoyen;
    }

    public void setTpsMoyen(int tpsMoyen) {
        this.tpsMoyen = tpsMoyen;
    }

    public int getNbJoueursMin() {
        return nbJoueursMin;
    }

    public void setNbJoueursMin(int nbJoueursMin) {
        this.nbJoueursMin = nbJoueursMin;
    }

    public int getNbJoueursMax() {
        return nbJoueursMax;
    }

    public void setNbJoueursMax(int nbJoueursMax) {
        this.nbJoueursMax = nbJoueursMax;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Jeu{");
        sb.append("id=").append(id);
        sb.append(", nom='").append(nom).append('\'');
        sb.append(", editeur='").append(editeur).append('\'');
        sb.append(", ageMin=").append(ageMin);
        sb.append(", tpsMoyen=").append(tpsMoyen);
        sb.append(", nbJoueursMin=").append(nbJoueursMin);
        sb.append(", nbJoueursMax=").append(nbJoueursMax);
        sb.append('}');
        return sb.toString();
    }
}
