import dao.HibernateUtil;
import dao.JeuDao;
import entity.Jeu;
import org.hibernate.Session;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class JeuDaoTest {

    @Test
    public void testHibernateConfig() throws Exception {
        Session currentSession = HibernateUtil.getCurrentSession();
        Assert.assertNotNull(currentSession);
    }
    @Test
    public void saveJeu() throws Exception {
        Jeu jeu = new Jeu();
        jeu.setNom("Legacy of Kain : Soul Reaver 2");
        jeu.setEditeur("Eidos");
        jeu.setAgeMin(10);
        jeu.setTpsMoyen(15);
        jeu.setNbJoueursMin(1);
        jeu.setNbJoueursMax(1);


        JeuDao dao = new JeuDao();
        HibernateUtil.getCurrentSession().beginTransaction();
        Integer id = dao.save(jeu);
        HibernateUtil.getCurrentSession().getTransaction().commit();

    }
    @Test
    public void updateJeu() throws Exception {
        Jeu jeu = new Jeu();
        jeu.setNom("Legacy of Kain : Soul Reaver 2");
        jeu.setEditeur("Eidos");
        jeu.setAgeMin(10);
        jeu.setTpsMoyen(15);
        jeu.setNbJoueursMin(1);
        jeu.setNbJoueursMax(1);

        JeuDao jeuDao = new JeuDao();
        HibernateUtil.getCurrentSession().beginTransaction();
        Integer id = jeuDao.save(jeu);
        HibernateUtil.getCurrentSession().getTransaction().commit();


        HibernateUtil.getCurrentSession().beginTransaction();
        Jeu loadedJeu = jeuDao.load(id);
        HibernateUtil.getCurrentSession().getTransaction().commit();

        Assert.assertNotNull(loadedJeu);
        Assert.assertEquals(jeu.getNom(), loadedJeu.getNom());

    }
@Test
    public void loadJeu() throws Exception {
        Jeu jeu = new Jeu();
        jeu.setNom("Legacy of Kain : Soul Reaver 2");
        jeu.setEditeur("Eidos");
        jeu.setAgeMin(10);
        jeu.setTpsMoyen(15);
        jeu.setNbJoueursMin(1);
        jeu.setNbJoueursMax(1);

        JeuDao jeuDao = new JeuDao();
        HibernateUtil.getCurrentSession().beginTransaction();
        Integer id = jeuDao.save(jeu);
        HibernateUtil.getCurrentSession().getTransaction().commit();


        HibernateUtil.getCurrentSession().beginTransaction();
        Jeu loadedJeu = jeuDao.load(id);
        HibernateUtil.getCurrentSession().getTransaction().commit();

        Assert.assertNotNull(loadedJeu);
        Assert.assertEquals(jeu.getNom(), loadedJeu.getNom());

    }

    @Test
    public void all() throws Exception {
        Jeu jeu1 = new Jeu();
        jeu1.setNom("Mario Party");
        jeu1.setEditeur("Nintendo");
        jeu1.setAgeMin(7);
        jeu1.setTpsMoyen(90);
        jeu1.setNbJoueursMin(1);
        jeu1.setNbJoueursMax(4);
        
        Jeu jeu2 = new Jeu();
        jeu2.setNom("Crash Team Racing");
        jeu2.setEditeur("Sony");
        jeu2.setAgeMin(7);
        jeu2.setTpsMoyen(125);
        jeu2.setNbJoueursMin(1);
        jeu2.setNbJoueursMax(4);
        
        Jeu jeu3 = new Jeu();
        jeu3.setNom("The Elder Scrolls III - Morrowind");
        jeu3.setEditeur("Bethesda");
        jeu3.setAgeMin(16);
        jeu3.setTpsMoyen(300);
        jeu3.setNbJoueursMin(1);
        jeu3.setNbJoueursMax(1);
        
        Jeu jeu4 = new Jeu();
        jeu4.setNom("Civilization V");
        jeu4.setEditeur("2k Games");
        jeu4.setAgeMin(12);
        jeu4.setTpsMoyen(10000);
        jeu4.setNbJoueursMin(1);
        jeu4.setNbJoueursMax(8);
        

        JeuDao jeuDao = new JeuDao();

        //on enregistre
        HibernateUtil.getCurrentSession().beginTransaction();
        jeuDao.save(jeu1);
        jeuDao.save(jeu2);
        jeuDao.save(jeu3);
        jeuDao.save(jeu4);
        HibernateUtil.getCurrentSession().getTransaction().commit();

        //on test qu'on récupère tout
        HibernateUtil.getCurrentSession().beginTransaction();
        List all = jeuDao.getAll();
        HibernateUtil.getCurrentSession().getTransaction().commit();

        Assert.assertNotNull(all);
        Assert.assertEquals(4, all.size());

    }
    
    @Test
    public void testFindByName() throws Exception {

        Jeu jeu4 = new Jeu();
        jeu4.setNom("Civilization V");
        jeu4.setEditeur("2k Games");
        jeu4.setAgeMin(12);
        jeu4.setTpsMoyen(10000);
        jeu4.setNbJoueursMin(1);
        jeu4.setNbJoueursMax(8);

        JeuDao jeuDao = new JeuDao();
        HibernateUtil.getCurrentSession().beginTransaction();
        jeuDao.save(jeu4);
        HibernateUtil.getCurrentSession().getTransaction().commit();


        HibernateUtil.getCurrentSession().beginTransaction();
        Jeu loadedJeu = jeuDao.getJeuFromName("Civilization V");
        HibernateUtil.getCurrentSession().getTransaction().commit();

        Assert.assertNotNull(loadedJeu);
        Assert.assertEquals(jeu4.getNom(), loadedJeu.getNom());

        HibernateUtil.getCurrentSession().beginTransaction();
        loadedJeu = jeuDao.getJeuFromName("Civilization V");
        HibernateUtil.getCurrentSession().getTransaction().commit();

        Assert.assertNull(loadedJeu);
    }

}
